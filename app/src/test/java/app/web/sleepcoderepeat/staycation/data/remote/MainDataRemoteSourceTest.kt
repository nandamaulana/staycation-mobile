package app.web.sleepcoderepeat.staycation.data.remote

import android.content.Context
import android.graphics.BitmapFactory
import android.os.Build
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.runner.AndroidJUnitRunner
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.api.ApiService
import app.web.sleepcoderepeat.staycation.api.dao.OrderDao
import app.web.sleepcoderepeat.staycation.utils.createTempFile
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.MultipartBody.Part.Companion.createFormData
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(maxSdk = Build.VERSION_CODES.P)
class MainDataRemoteSourceTest{

    private val apiService = ApiService.create()

    @Test
    fun getLandingPageData() {
        try {
            val call = apiService.getLandingPage().blockingSingle()
            println("hero : ${call.hero}")
            assertTrue(call.hero.toString().isNotBlank())

        } catch (e: Exception) {
            println("Error : ${e.message}")
            e.printStackTrace()
        }
    }

    @Test
    fun getDetailPageData() {
        try {
            val call = apiService.getDetailPage(ID_DETAIL).blockingSingle()
            assertTrue(call.title.isNotBlank())

        } catch (e: Exception) {
            println("Error : ${e.message}")
            e.printStackTrace()
        }

    }

    @Test
    fun submitCheckoutData() {
        try {
            val orderDaoMap = HashMap<String, RequestBody>()
            val file = BitmapFactory.decodeResource(getInstrumentation().context.resources, R.drawable.unit_test_upload).createTempFile(getInstrumentation().context)!!
            val fileReqBody: RequestBody = file.asRequestBody("image/jpg".toMediaTypeOrNull())
            val image: MultipartBody.Part =
                createFormData("image", file.name, fileReqBody)
            with(orderDao) {
                orderDaoMap["idItem"] = idItem.toRequestBody("text/plain".toMediaTypeOrNull())
                orderDaoMap["duration"] = duration.toRequestBody("text/plain".toMediaTypeOrNull())
                orderDaoMap["bookingStartDate"] =
                    bookingStartDate.toRequestBody("text/plain".toMediaTypeOrNull())
                orderDaoMap["bookingEndDate"] =
                    bookingEndDate.toRequestBody("text/plain".toMediaTypeOrNull())
                orderDaoMap["firstName"] = firstName.toRequestBody("text/plain".toMediaTypeOrNull())
                orderDaoMap["lastName"] = lastName.toRequestBody("text/plain".toMediaTypeOrNull())
                orderDaoMap["email"] = email.toRequestBody("text/plain".toMediaTypeOrNull())
                orderDaoMap["phoneNumber"] =
                    phoneNumber.toRequestBody("text/plain".toMediaTypeOrNull())
                orderDaoMap["accountHolder"] =
                    accountHolder.toRequestBody("text/plain".toMediaTypeOrNull())
                orderDaoMap["bankFrom"] = bankFrom.toRequestBody("text/plain".toMediaTypeOrNull())
            }
            val call = apiService.submitBooking(orderDaoMap,image).execute()
            assertTrue(call.isSuccessful)

        } catch (e: Exception) {
            println("Error : ${e.message}")
            e.printStackTrace()
        }

    }

    companion object {
        const val ID_DETAIL = "5e96cbe292b97300fc902222"
        val orderDao = OrderDao(
            "5e96cbe292b97300fc902222",
            "4",
            "2020-04-04",
            "2020-04-08",
            "Nanda",
            "Maulana",
            "nandarm.sti@gmail.com",
            "083159730129",
            "Nanda Maulana",
            "Mandiri"
        )
    }
}
