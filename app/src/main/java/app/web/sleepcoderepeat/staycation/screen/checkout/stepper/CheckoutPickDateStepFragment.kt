package app.web.sleepcoderepeat.staycation.screen.checkout.stepper

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.UiThread
import androidx.core.util.Pair
import androidx.fragment.app.Fragment
import app.web.sleepcoderepeat.staycation.databinding.FragmentCheckoutStepPickDateBinding
import app.web.sleepcoderepeat.staycation.screen.checkout.CheckoutActivity
import app.web.sleepcoderepeat.staycation.utils.setMinDate
import app.web.sleepcoderepeat.staycation.utils.setNoon
import app.web.sleepcoderepeat.staycation.utils.toDate
import com.google.android.material.datepicker.MaterialDatePicker
import com.stepstone.stepper.BlockingStep
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import java.util.*

internal class CheckoutPickDateStepFragment : Fragment(), BlockingStep {

    lateinit var viewBinding: FragmentCheckoutStepPickDateBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewBinding = FragmentCheckoutStepPickDateBinding.inflate(inflater).apply {
            vm = (activity as CheckoutActivity).obtainViewModel()
        }
        viewBinding.lifecycleOwner = this

        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupDateRangePicker()

    }

    private fun setupDateRangePicker(
        now: Calendar = Calendar.getInstance().setNoon(),
        tomorrow: Calendar = Calendar.getInstance().setNoon()
    ) {
        val builder = MaterialDatePicker.Builder.dateRangePicker()
        if (now == tomorrow)
            tomorrow.add(Calendar.DATE, 1)
        builder.setSelection(Pair(now.timeInMillis, tomorrow.timeInMillis))
        builder.setMinDate(now)
        val picker = builder.build()
        viewBinding.datePickerArea.setOnClickListener {
            picker.show(
                activity?.supportFragmentManager!!,
                picker.toString()
            )
        }
        picker.addOnNegativeButtonClickListener { picker.dismiss() }
        picker.addOnPositiveButtonClickListener {
            viewBinding.vm?.setSelectedDate(it.first!!, it.second!!)
        }

        viewBinding.vm!!.apply {
            checkoutDateRange.observe(
                viewLifecycleOwner,
                androidx.lifecycle.Observer {
                    setupDateRangePicker(Calendar.getInstance().run {
                        time = orderDataField.value!!.bookingStartDate.toDate()
                        setNoon()
                        this
                    }, Calendar.getInstance().run {
                        time = orderDataField.value!!.bookingEndDate.toDate()
                        setNoon()
                        this
                    })
                    viewBinding.dateRangeText.text = getCheckoutDate()
                })
        }
    }

    @UiThread
    override fun onBackClicked(callback: StepperLayout.OnBackClickedCallback) {
        callback.goToPrevStep()
    }

    override fun onSelected() { }

    override fun onCompleteClicked(callback: StepperLayout.OnCompleteClickedCallback) {
        callback.complete()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    @UiThread
    override fun onNextClicked(callback: StepperLayout.OnNextClickedCallback) {
        callback.goToNextStep()
    }

    override fun verifyStep(): VerificationError? {
        return null
    }

    override fun onError(error: VerificationError) { }

}