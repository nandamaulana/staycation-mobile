package app.web.sleepcoderepeat.staycation.screen.main


import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import app.web.sleepcoderepeat.staycation.databinding.FragmentMainBinding
import kotlin.math.abs

class MainFragment : Fragment() {

    private lateinit var viewBinding: FragmentMainBinding
    private lateinit var imageSliderAdapter: MainImageSliderAdapter
    private var sliderHandler: Handler = Handler()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewBinding = FragmentMainBinding.inflate(inflater).apply {
            vm = (activity as MainActivity).obtainViewModel()
        }
        viewBinding.lifecycleOwner = this
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupImageSlider()
        setupCategoryRecyclerView()
        with(viewBinding.swipeRefresh) {
            setOnRefreshListener {
                viewBinding.vm?.start()
                isRefreshing = false
            }
        }
        viewBinding.vm?.start()
    }

    private fun setupImageSlider() {
        val viewModel = viewBinding.vm
        if (viewModel != null) {
            imageSliderAdapter =
                MainImageSliderAdapter(viewModel.mostPicked, viewModel, viewBinding.vpImageSlider)
            with(viewBinding.vpImageSlider) {
                adapter = imageSliderAdapter
                clipToPadding = false
                clipChildren = false
                offscreenPageLimit = 3
                getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
            }
            val compositePageTransformer = CompositePageTransformer()
            with(compositePageTransformer) {
                addTransformer(MarginPageTransformer(40))
                addTransformer { page, position ->
                    val r: Float = 1 - abs(position)
                    page.scaleY = (0.85f + r * 0.15f)
                }
            }
            with(viewBinding.vpImageSlider) {
                setPageTransformer(compositePageTransformer)
                registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        super.onPageSelected(position)
                        sliderHandler.removeCallbacks(sliderRunnable)
                        sliderHandler.postDelayed(sliderRunnable, 3000)
                    }
                })
            }
        }
    }

    private fun setupCategoryRecyclerView() {
        val viewModel = viewBinding.vm
        if (viewModel != null) {
            viewBinding.rvCategory.adapter = MainCategoryAdapter(viewModel.category, viewModel)
        }
    }

    private var sliderRunnable: Runnable = Runnable {
        if (viewBinding.vpImageSlider.currentItem == imageSliderAdapter.itemCount - 1)
            viewBinding.vpImageSlider.currentItem = 0
        else
            viewBinding.vpImageSlider.currentItem = viewBinding.vpImageSlider.currentItem + 1
    }

    override fun onResume() {
        super.onResume()
        sliderHandler.postDelayed(sliderRunnable, 3000)
//        viewBinding.vm?.start()
    }

    override fun onPause() {
        super.onPause()
        sliderHandler.removeCallbacks(sliderRunnable)
    }

    companion object {
        fun newInstance() = MainFragment()
    }


}
