package app.web.sleepcoderepeat.staycation.data

import app.web.sleepcoderepeat.staycation.api.dao.OrderDao
import app.web.sleepcoderepeat.staycation.api.dao.DetailDao
import app.web.sleepcoderepeat.staycation.api.dao.LandingDao
import java.io.File

interface MainDataSource {
    fun getLandingPageData(callback: GetLandingPageDataCallback)
    fun getDetailPageData(callback: GetDetailPageDataCallback, _id: String)
    fun submitCheckoutData(callback: SubmitCheckoutDataCallback, orderDao: OrderDao, file: File)

    interface GetLandingPageDataCallback {
        fun onDataLoaded(landingDao: LandingDao?)
        fun onNotAvailable()
        fun onError(msg: String?)
    }

    interface GetDetailPageDataCallback {
        fun onDataLoaded(detailDao: DetailDao?)
        fun onNotAvailable()
        fun onError(msg: String?)
    }

    interface SubmitCheckoutDataCallback {
        fun onDataSubmitted(response: String)
        fun onNotAvailable()
        fun onError(msg: String?)
    }
}