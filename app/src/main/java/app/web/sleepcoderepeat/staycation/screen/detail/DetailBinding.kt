package app.web.sleepcoderepeat.staycation.screen.detail

import android.util.Log
import android.webkit.WebView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import app.web.sleepcoderepeat.staycation.api.dataclass.Feature
import app.web.sleepcoderepeat.staycation.api.dataclass.Image
import com.facebook.shimmer.ShimmerFrameLayout

object DetailBinding {
    @BindingAdapter("app:isLoading")
    @JvmStatic
    fun setIsLoading(shimmerFrameLayout: ShimmerFrameLayout, isLoading: Boolean) {

        if (isLoading) {
            shimmerFrameLayout.startShimmerAnimation()
            return;
        }
        shimmerFrameLayout.stopShimmerAnimation()
    }

    @BindingAdapter("app:pagerAdapterDetail")
    @JvmStatic
    fun setPagerAdapter(viewPager2: ViewPager2, items: MutableList<Image>) {
        (viewPager2.adapter as DetailImageSliderAdapter).replaceData(items)
    }

    @BindingAdapter("app:benefitList")
    @JvmStatic
    fun setBenefitList(recyclerView: RecyclerView, items: MutableList<Feature>) {
        (recyclerView.adapter as DetailBenefitAdapter).replaceData(items)
    }

    @BindingAdapter("app:loadBodyChildEncoded")
    @JvmStatic
    fun setBodyChildEncoded(webView: WebView, child: String?) {
        if (child != null)
            webView.loadDataWithBaseURL(
                null,
                "<html><body>$child$child$child</body></html>",
                "text/html",
                "utf-8",
                null
            )
    }

}