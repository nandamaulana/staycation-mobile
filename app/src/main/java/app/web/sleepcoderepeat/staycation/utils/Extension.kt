package app.web.sleepcoderepeat.staycation.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import android.util.Log
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.web.sleepcoderepeat.staycation.R
import com.google.android.material.datepicker.*
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


//fun AppCompatActivity.setupActionBar(@IdRes toolbarId: Int, action: ActionBar.() -> Unit) {
//    setSupportActionBar(findViewById(toolbarId))
//    supportActionBar?.run {
//        action()
//    }
//}
fun AppCompatActivity.replaceFragmentInActivity(fragment: Fragment, frameId: Int) {
    supportFragmentManager.transact {
        replace(frameId, fragment)
    }
}

private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commit()
}

fun <T : ViewModel> AppCompatActivity.obtainViewModel(viewModelClass: Class<T>) =
    ViewModelProvider(this, ViewModelFactory.getInstance(application)).get(viewModelClass)


fun AppCompatActivity.addFragmentToActivity(fragment: Fragment, tag: String) {
    supportFragmentManager.transact {
        add(fragment, tag)
    }
}

val Context.picasso: Picasso
    get() = Picasso.get()

fun ImageView.load(path: String, request: (RequestCreator) -> RequestCreator) {
    request(context.picasso.load(path)).into(this)
}

@SuppressLint("SimpleDateFormat")
fun Date.toStringFormatted(dateFormat: String = "yyyy-MM-dd"): String {
    return try {
        SimpleDateFormat(dateFormat).format(this)
    } catch (e: Exception) {
        throw RuntimeException("${e.message}")
    }
}

@SuppressLint("SimpleDateFormat")
fun String.toDate(dateFormat: String = "yyyy-MM-dd"): Date {
    return try {
        SimpleDateFormat(dateFormat).parse(this)
    } catch (e: Exception) {
        throw RuntimeException("${e.message}")
    }
}

@SuppressLint("SimpleDateFormat")
fun Date.sumDays(sumValue: Int): Date {
    val calendar = Calendar.getInstance()
    with(calendar) {
        time = this@sumDays
        add(Calendar.DATE, sumValue)
    }
    return calendar.time
}

@SuppressLint("SimpleDateFormat")
fun Date.dayDiff(secondDate: Date): Int {
    return TimeUnit.DAYS.convert(this.time - secondDate.time, TimeUnit.MILLISECONDS).toInt()
}

@SuppressLint("SimpleDateFormat")
fun Date.getDateRangeWith(endDate: Date): String {
    return "${this.toStringFormatted("dd MMM")} - ${endDate.toStringFormatted("dd MMM")}"
}

@SuppressLint("SimpleDateFormat")
fun Calendar.setNoon(): Calendar {
    this.set(Calendar.HOUR_OF_DAY, 12)
    this.set(Calendar.MINUTE, 0)
    this.set(Calendar.SECOND, 0)
    this.set(Calendar.MILLISECOND, 0)
    return this
}

fun Bitmap.createTempFile(context: Context): File? {
    val file = File(
        context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        , "staycation_${System.currentTimeMillis()}.jpg"
    )
    val bos = ByteArrayOutputStream()
    compress(Bitmap.CompressFormat.JPEG, 100, bos)
    val bitmapData: ByteArray = bos.toByteArray()
    //write the bytes in file
    try {
        val fos = FileOutputStream(file)
        fos.write(bitmapData)
        fos.flush()
        fos.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return file
}

fun <T> MaterialDatePicker.Builder<T>.setMinDate(calendarStart: Calendar) {
    val constraintsBuilder = CalendarConstraints.Builder()
    val calendarEnd = Calendar.getInstance()
    calendarEnd.add(Calendar.YEAR, 5)
    calendarStart.add(Calendar.DATE, -1)
    val validators: ArrayList<CalendarConstraints.DateValidator> = ArrayList()
    validators.add(DateValidatorPointForward.from(calendarStart.timeInMillis))
    validators.add(DateValidatorPointBackward.before(calendarEnd.timeInMillis))
    constraintsBuilder.setValidator(CompositeDateValidator.allOf(validators))
    this.setCalendarConstraints(constraintsBuilder.build())
}

@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, url: String?) {
    if (url != null) {
        if (view.id == R.id.bankImage) {
            view.load(Constant.BASE_URL + url) { requestCreator ->
                requestCreator.resize(
                    view.maxWidth,
                    view.maxHeight
                )
            }
            return
        }
        view.load(Constant.BASE_URL + url) { requestCreator -> requestCreator.fit().centerCrop() }
    }
}