package app.web.sleepcoderepeat.staycation.utils

import android.content.Context
import androidx.preference.PreferenceManager
import app.web.sleepcoderepeat.staycation.data.MainDataRepository
import app.web.sleepcoderepeat.staycation.data.local.MainDataLocalSource
import app.web.sleepcoderepeat.staycation.data.remote.MainDataRemoteSource

object Injection {
    fun providedMainDataRepository(context: Context) = MainDataRepository.getInstance(
        MainDataRemoteSource,
            MainDataLocalSource.getInstance(PreferenceManager.getDefaultSharedPreferences(context))!!)
}