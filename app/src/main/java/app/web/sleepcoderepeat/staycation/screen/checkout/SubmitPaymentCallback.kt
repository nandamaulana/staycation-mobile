package app.web.sleepcoderepeat.staycation.screen.checkout

interface SubmitPaymentCallback {
    fun onSuccess(message:String)
    fun onError(message: String?)
}