package app.web.sleepcoderepeat.staycation.screen.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.api.dataclass.Image
import app.web.sleepcoderepeat.staycation.databinding.DetailImageSliderItemBinding

class DetailImageSliderAdapter(
    private var image: MutableList<Image>,
    private var detailViewModel: DetailViewModel,
    private var viewPager2: ViewPager2
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val imageSliderBinding: DetailImageSliderItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.detail_image_slider_item, parent, false
        )
        return DetailImageSliderHolder(imageSliderBinding)
    }

    override fun getItemCount(): Int = image.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = image[position]

        (holder as DetailImageSliderHolder).bindItem(item)
    }

    fun replaceData(items: MutableList<Image>) {
        setList(items)
    }

    private fun setList(item: MutableList<Image>) {
        this.image = item
                notifyDataSetChanged()
    }

    class DetailImageSliderHolder(var binding: DetailImageSliderItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindItem(image: Image) {
            binding.datas = image
            binding.executePendingBindings()
        }
    }
}