package app.web.sleepcoderepeat.staycation.screen.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.api.dataclass.Item
import app.web.sleepcoderepeat.staycation.databinding.CategoryItemBinding

class MainCategoryItemAdapter(
    private var categoryItem: MutableList<Item>,
    private var mainViewModel: MainViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CategoryItemHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.category_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = categoryItem.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = categoryItem[position]
        val actionListener = object : MainActionListener {
            override fun onItemClick() {
                mainViewModel.openDetail(item._id)
            }
        }
        (holder as CategoryItemHolder).bindItem(item, actionListener)
    }

    fun replaceData(items: MutableList<Item>) {
        setList(items)
    }

    private fun setList(item: MutableList<Item>) {
        this.categoryItem = item
        notifyDataSetChanged()
    }

    class CategoryItemHolder(var binding: CategoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(item: Item, mainActionListener: MainActionListener) {
            binding.datas = item
            binding.action = mainActionListener
            binding.executePendingBindings()
        }
    }
}