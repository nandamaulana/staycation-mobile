package app.web.sleepcoderepeat.staycation.screen.checkout.stepper

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.UiThread
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import app.web.sleepcoderepeat.staycation.databinding.FragmentCheckoutStepSubmitPaymentBinding
import app.web.sleepcoderepeat.staycation.screen.checkout.CheckoutActivity
import app.web.sleepcoderepeat.staycation.screen.checkout.CheckoutBankListAdapter
import app.web.sleepcoderepeat.staycation.screen.checkout.SubmitPaymentCallback
import app.web.sleepcoderepeat.staycation.screen.complete.CompleteActivity
import app.web.sleepcoderepeat.staycation.utils.FileUtils
import app.web.sleepcoderepeat.staycation.utils.createTempFile
import com.stepstone.stepper.BlockingStep
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError

internal class CheckoutSubmitPaymentStepFragment : Fragment(), BlockingStep {

    lateinit var viewBinding: FragmentCheckoutStepSubmitPaymentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewBinding = FragmentCheckoutStepSubmitPaymentBinding.inflate(inflater).apply {
            vm = (activity as CheckoutActivity).obtainViewModel()
        }
        viewBinding.lifecycleOwner = this

        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupBankAdapter()
        setupImagePicker()
    }

    private fun setupImagePicker() {
        viewBinding.etPickImage.setOnClickListener {
            if (checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED
            ) {
                //permission denied
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                //show popup to request runtime permission
                requestPermissions(permissions, Companion.PERMISSION_CODE);
            } else {
                //permission already granted
                pickImageFromGallery();
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    //permission from popup granted
                    pickImageFromGallery()
                } else {
                    Toast.makeText(requireActivity(), "Permission denied", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE) {
            viewBinding.vm!!.setPaymentProofFile(
                BitmapFactory.decodeFile(
                    FileUtils().getFile(
                        requireContext(),
                        data!!.data!!
                    )!!.path
                ).createTempFile(requireContext())!!
            )
        }
    }


    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, PICK_IMAGE)
    }

    private fun setupBankAdapter() {
        val viewModel = viewBinding.vm
        if (viewModel != null) {
            viewBinding.rvBank.adapter = CheckoutBankListAdapter(viewModel.bankList)
        }
    }

    @UiThread
    override fun onBackClicked(callback: StepperLayout.OnBackClickedCallback) {
        callback.goToPrevStep()
    }

    override fun onSelected() {}

    override fun onCompleteClicked(callback: StepperLayout.OnCompleteClickedCallback) {

        callback.stepperLayout.showProgress("Operation in progress, please wait...")
        with(viewBinding) {
            vm!!.submitPayment(
                etBankOrigin.text.toString(),
                etAccountHolder.text.toString(),
                object : SubmitPaymentCallback {
                    override fun onSuccess(message: String) {
                        callback.stepperLayout.hideProgress()
                        startActivity(Intent(requireContext(),CompleteActivity::class.java))
                    }

                    override fun onError(message: String?) {
                        callback.stepperLayout.hideProgress()
                    }
                })
        }
    }

    @UiThread
    override fun onNextClicked(callback: StepperLayout.OnNextClickedCallback) {
        callback.goToNextStep()
    }

    override fun verifyStep(): VerificationError? {
        return null
    }

    private val isValid: Boolean
        get() {
            return false
        }

    override fun onError(error: VerificationError) {}

    companion object {
        private const val PERMISSION_CODE = 100
        private const val PICK_IMAGE = 200
    }
}