package app.web.sleepcoderepeat.staycation.screen.checkout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.utils.obtainViewModel
import app.web.sleepcoderepeat.staycation.utils.replaceFragmentInActivity

class CheckoutActivity : AppCompatActivity() {

    private lateinit var cActivity: AppCompatActivity
    private lateinit var viewModel: CheckoutViewModel
    var checkoutFragment = CheckoutFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)

        setupFragment()
        setupViewModel()
    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frameCheckout)
        replaceFragmentInActivity(checkoutFragment, R.id.frameCheckout)
    }
    private fun setupViewModel() {
        viewModel = obtainViewModel()
    }

    override fun onBackPressed() {
        checkoutFragment.apply {

            if (viewBinding.stepperLayout.currentStepPosition>0)
                viewBinding.stepperLayout.onBackClicked()
            else
                activity?.finish()
        }
    }

    fun obtainViewModel(): CheckoutViewModel = obtainViewModel(CheckoutViewModel::class.java)
}
