package app.web.sleepcoderepeat.staycation.screen.detail

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import app.web.sleepcoderepeat.staycation.api.dao.DetailDao
import app.web.sleepcoderepeat.staycation.api.dataclass.Bank
import app.web.sleepcoderepeat.staycation.api.dataclass.Feature
import app.web.sleepcoderepeat.staycation.api.dataclass.Image
import app.web.sleepcoderepeat.staycation.api.dataclass.Item
import app.web.sleepcoderepeat.staycation.data.MainDataRepository
import app.web.sleepcoderepeat.staycation.data.MainDataSource
import app.web.sleepcoderepeat.staycation.utils.SingleLiveEvent


class DetailViewModel(
    application: Application,
    private val mainDataRepository: MainDataRepository
) :
    AndroidViewModel(application) {

    val detailDataField: ObservableField<DetailDao> = ObservableField()
    val isLoading = MutableLiveData<Boolean>()
    val hasBenefit = MutableLiveData<Boolean>()
    val imageId: ObservableList<Image> = ObservableArrayList()
    val benefit: ObservableList<Feature> = ObservableArrayList()
    val openCheckout = SingleLiveEvent<Item>()

    fun start(_id: String) {
        getDetailData(_id)
    }

    fun getItem(): Item {
        val temp = detailDataField.get()
        return Item(
            temp!!._id,
            temp.country,
            temp.unit,
            temp.imageId,
            temp.title,
            temp.price,
            temp.city,
            temp.description,
            temp.isPopular,
            temp.sumBooking,
            temp.featureId,
            temp.activityId
        )
    }

    fun openCheckout(item: Item) {
        openCheckout.value = item
    }

    private fun getDetailData(_id: String) {
        hasBenefit.value = false
        isLoading.value = true
        mainDataRepository.getDetailPageData(object :   MainDataSource.GetDetailPageDataCallback {
            override fun onDataLoaded(detailDao: DetailDao?) {
                isLoading.value = false
                detailDataField.set(detailDao)
                with(imageId) {
                    clear()
                    addAll(detailDao!!.imageId)
                }
                with(benefit) {
                    clear()
                    addAll(detailDao!!.featureId)
                }
                if (benefit.size > 0) {
                    hasBenefit.value = true
                }
            }

            override fun onError(msg: String?) {
                isLoading.value = false
                Toast.makeText(getApplication(), "Error: $msg", Toast.LENGTH_LONG).show()
            }

            override fun onNotAvailable() {
                isLoading.value = false
                Toast.makeText(getApplication(), "Data not available", Toast.LENGTH_LONG).show()
            }
        }, _id)
    }

}