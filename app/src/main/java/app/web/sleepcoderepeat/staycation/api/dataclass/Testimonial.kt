package app.web.sleepcoderepeat.staycation.api.dataclass

import java.io.Serializable

data class Testimonial(
    val _id: String,
    val imageUrl: String,
    val name: String,
    val rate: Float,
    val content: String,
    val familyName: String,
    val familyOccupation: String
) : Serializable