package app.web.sleepcoderepeat.staycation.data.local

import android.content.SharedPreferences
import androidx.annotation.VisibleForTesting
import app.web.sleepcoderepeat.staycation.api.dao.OrderDao
import app.web.sleepcoderepeat.staycation.data.MainDataSource
import java.io.File

class MainDataLocalSource private constructor(private val preferences: SharedPreferences) :
    MainDataSource {
    override fun getLandingPageData(callback: MainDataSource.GetLandingPageDataCallback) {

    }

    override fun getDetailPageData(
        callback: MainDataSource.GetDetailPageDataCallback,
        _id: String
    ) {    }

    override fun submitCheckoutData(
        callback: MainDataSource.SubmitCheckoutDataCallback,
        orderDao: OrderDao,
        file: File
    ) {
    }

    companion object {
        private var INSTANCE: MainDataLocalSource? = null

        @JvmStatic
        fun getInstance(preferences: SharedPreferences): MainDataLocalSource? {
            if (INSTANCE == null) {
                synchronized(MainDataLocalSource::class.java) {
                    INSTANCE = MainDataLocalSource(preferences)
                }
            }
            return INSTANCE
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }
}