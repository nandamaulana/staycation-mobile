package app.web.sleepcoderepeat.staycation.screen.checkout.stepper

import android.content.Context
import android.util.Log
import androidx.fragment.app.FragmentManager
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.screen.checkout.CheckoutViewModel.Companion.StepData
import com.stepstone.stepper.Step
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter
import com.stepstone.stepper.viewmodel.StepViewModel

class CheckoutStepperAdapter(
    fm: FragmentManager,
    private val stepDataList: MutableList<StepData>,
    context: Context
) : AbstractFragmentStepAdapter(fm, context) {

    override fun getViewModel(position: Int): StepViewModel {
        return StepViewModel.Builder(context)
            .setBackButtonStartDrawableResId(R.drawable.ic_arrow_back_black_24dp)
            .setBackButtonLabel(if(position == 0) "Cancel" else "Back")
            .setTitle(stepDataList[position].titleStep)
            .setEndButtonLabel(stepDataList[position].nextText)
            .setNextButtonEndDrawableResId( if(stepDataList.size != position+1)  R.drawable.ic_arrow_next_black_24dp else R.drawable.ic_file_upload_black_24dp)
            .create()
    }

    override fun getCount(): Int = stepDataList.size


    override fun createStep(position: Int): Step = stepDataList[position].step

}