package app.web.sleepcoderepeat.staycation.api.dataclass

import java.io.Serializable

data class Category(
    val _id: String,
    val itemId: MutableList<Item>,
    val name: String
) : Serializable