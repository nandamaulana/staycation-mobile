package app.web.sleepcoderepeat.staycation.screen.main

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.api.dataclass.Category
import app.web.sleepcoderepeat.staycation.databinding.CategoryBinding

class MainCategoryAdapter(
    private var category: MutableList<Category>,
    private var mainViewModel: MainViewModel

) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CategoryHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.category,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = category.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = category[position]
        (holder as CategoryHolder).bindItem(item, mainViewModel)

    }

    fun replaceData(items: MutableList<Category>) {
        setList(items)
    }

    private fun setList(item: MutableList<Category>) {
        this.category = item
        notifyDataSetChanged()
    }

    class CategoryHolder(var binding: CategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(category: Category, mainViewModel: MainViewModel) {
            binding.datas = category
            binding.rvCategoryItem.adapter = MainCategoryItemAdapter(category.itemId,mainViewModel)
            binding.executePendingBindings()
        }
    }
}