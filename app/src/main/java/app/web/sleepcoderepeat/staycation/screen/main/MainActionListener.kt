package app.web.sleepcoderepeat.staycation.screen.main

interface MainActionListener{
    fun onItemClick()
}