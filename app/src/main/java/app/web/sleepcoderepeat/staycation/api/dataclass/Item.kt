package app.web.sleepcoderepeat.staycation.api.dataclass

import java.io.Serializable

data class Item(
    val _id: String,
    val country: String,
    val unit: String,
    val imageId: MutableList<Image>,
    val title: String,
    val price: String,
    val city: String,
    val description: String,
    val isPopular: Boolean,
    val sumBooking: Int,
    val feature: MutableList<Feature>,
    val activity: MutableList<Activity>
) : Serializable