package app.web.sleepcoderepeat.staycation.api.dao

import app.web.sleepcoderepeat.staycation.api.dataclass.*

data class DetailDao(
    val country: String,
    val isPopular: Boolean,
    val unit: String,
    val sumBooking : Int,
    val imageId: MutableList<Image>,
    val featureId: MutableList<Feature>,
    val activityId: MutableList<Activity>,
    val _id : String,
    val title : String,
    val price : String,
    val city : String,
    val description : String,
    val categoryId : String,
    val bank: MutableList<Bank>,
    val testimonial: Testimonial
)