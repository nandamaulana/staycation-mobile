package app.web.sleepcoderepeat.staycation.screen.main

import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import app.web.sleepcoderepeat.staycation.api.dataclass.Category
import app.web.sleepcoderepeat.staycation.api.dataclass.Item
import com.facebook.shimmer.ShimmerFrameLayout

object MainBinding {
    @BindingAdapter("app:isLoading")
    @JvmStatic
    fun setIsLoading(shimmerFrameLayout: ShimmerFrameLayout, isLoading:Boolean){

        if (isLoading){
            shimmerFrameLayout.startShimmerAnimation()
            return;
        }
        shimmerFrameLayout.stopShimmerAnimation()
    }

    @BindingAdapter("app:pagerAdapter")
    @JvmStatic
    fun setPagerAdapter(viewPager2: ViewPager2, items: MutableList<Item>){
        (viewPager2.adapter as MainImageSliderAdapter).replaceData(items)
    }

    @BindingAdapter("app:categoryList")
    @JvmStatic
    fun setCategoryList(recyclerView: RecyclerView, category: MutableList<Category>){
        with(recyclerView.adapter as MainCategoryAdapter){
            replaceData(category)
        }
    }
    @BindingAdapter("app:categoryItemList")
    @JvmStatic
    fun setCategoryItemList(recyclerView: RecyclerView, categoryItem: MutableList<Item>){
        with(recyclerView.adapter as MainCategoryItemAdapter){
            replaceData(categoryItem)
        }
    }


}