package app.web.sleepcoderepeat.staycation.data

import app.web.sleepcoderepeat.staycation.api.dao.OrderDao
import app.web.sleepcoderepeat.staycation.api.dao.DetailDao
import app.web.sleepcoderepeat.staycation.api.dao.LandingDao
import app.web.sleepcoderepeat.staycation.data.local.MainDataLocalSource
import java.io.File


class MainDataRepository(
    private val remoteDataSource: MainDataSource,
    private val localDataSource: MainDataSource
) : MainDataSource {
    override fun getLandingPageData(callback: MainDataSource.GetLandingPageDataCallback) {
        remoteDataSource.getLandingPageData(object : MainDataSource.GetLandingPageDataCallback {
            override fun onNotAvailable() {
                callback.onNotAvailable()
            }

            override fun onError(msg: String?) {
                callback.onError(msg)
            }

            override fun onDataLoaded(landingDao: LandingDao?) {
                callback.onDataLoaded(landingDao)
            }
        })
    }

    override fun getDetailPageData(
        callback: MainDataSource.GetDetailPageDataCallback,
        _id: String
    ) {
        remoteDataSource.getDetailPageData(object : MainDataSource.GetDetailPageDataCallback {
            override fun onNotAvailable() {
                callback.onNotAvailable()
            }

            override fun onError(msg: String?) {
                callback.onError(msg)
            }

            override fun onDataLoaded(detailDao: DetailDao?) {
                callback.onDataLoaded(detailDao)
            }
        }, _id)
    }

    override fun submitCheckoutData(
        callback: MainDataSource.SubmitCheckoutDataCallback,
        orderDao: OrderDao,
        file: File
    ) {
        remoteDataSource.submitCheckoutData(object : MainDataSource.SubmitCheckoutDataCallback {
            override fun onDataSubmitted(response: String) {
                callback.onDataSubmitted(response)
            }

            override fun onNotAvailable() {
                callback.onNotAvailable()
            }

            override fun onError(msg: String?) {
                callback.onError(msg)
            }
        },orderDao,file)
    }

    companion object {
        private var INSTANCE: MainDataRepository? = null

        @JvmStatic
        fun getInstance(
            mainDataRemoteSource: MainDataSource,
            newsLocalDataSource: MainDataLocalSource
        ) =
            INSTANCE ?: synchronized(MainDataRepository::class.java) {
                INSTANCE ?: MainDataRepository(mainDataRemoteSource, mainDataRemoteSource)
                    .also { INSTANCE = it }

            }

        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }

}