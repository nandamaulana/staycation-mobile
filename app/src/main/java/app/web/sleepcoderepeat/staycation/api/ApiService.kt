package app.web.sleepcoderepeat.staycation.api

import app.web.sleepcoderepeat.staycation.api.dao.DetailDao
import app.web.sleepcoderepeat.staycation.api.dao.LandingDao
import app.web.sleepcoderepeat.staycation.utils.Constant
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface ApiService {
    @GET("landing-page")
    fun getLandingPage(): Observable<LandingDao>

    @GET("detail-page/{id}")
    fun getDetailPage(
        @Path("id") id: String
    ): Observable<DetailDao>

    @Multipart
    @POST("booking-page")
    fun submitBooking(
        @PartMap data: HashMap<String, RequestBody>,
        @Part file: MultipartBody.Part
    ): Call<ResponseBody>

    companion object Factory {
        fun create(): ApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client =
                OkHttpClient.Builder().addInterceptor(interceptor).build()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create()
                )
                .client(client)
                .addConverterFactory(
                    GsonConverterFactory.create()
                )
                .baseUrl(Constant.BASE_ENDPOINT)
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}