package app.web.sleepcoderepeat.staycation.api.dataclass

import java.io.Serializable

data class Hero(
    val travelers: Int,
    val treasures: Int,
    val cities: Int
) : Serializable