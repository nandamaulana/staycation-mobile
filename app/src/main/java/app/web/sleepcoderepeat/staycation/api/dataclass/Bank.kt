package app.web.sleepcoderepeat.staycation.api.dataclass

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

data class Bank(
    val _id: String?,
    val nameBank: String?,
    val nomorRekening: String?,
    val nameOwner : String?,
    val imageUrl: String?
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_id)
        parcel.writeString(nameBank)
        parcel.writeString(nomorRekening)
        parcel.writeString(nameOwner)
        parcel.writeString(imageUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Bank> {
        override fun createFromParcel(parcel: Parcel): Bank {
            return Bank(parcel)
        }

        override fun newArray(size: Int): Array<Bank?> {
            return arrayOfNulls(size)
        }
    }
}