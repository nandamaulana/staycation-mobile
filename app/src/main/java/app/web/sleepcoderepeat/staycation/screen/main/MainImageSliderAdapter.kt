package app.web.sleepcoderepeat.staycation.screen.main

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.api.dataclass.Item
import app.web.sleepcoderepeat.staycation.databinding.ImageSliderItemBinding

class MainImageSliderAdapter(
    private var mostPicked: MutableList<Item>,
    private var mainViewModel: MainViewModel,
    private var viewPager2: ViewPager2
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val imageSliderBinding: ImageSliderItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.image_slider_item, parent, false
        )
        return ImageSliderHolder(imageSliderBinding)
    }

    override fun getItemCount(): Int = mostPicked.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = mostPicked[position]
        val actionListener = object : MainActionListener {
            override fun onItemClick() {
                mainViewModel.openDetail(item._id)
            }
        }
        (holder as ImageSliderHolder).bindItem(item, actionListener)
    }

    fun replaceData(items: MutableList<Item>) {
        setList(items)
    }

    private fun setList(item: MutableList<Item>) {
        this.mostPicked = item
        notifyDataSetChanged()
    }

    class ImageSliderHolder(var binding: ImageSliderItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindItem(item: Item, mainActionListener: MainActionListener) {
            binding.datas = item
            binding.action = mainActionListener
            binding.executePendingBindings()
        }
    }
}