package app.web.sleepcoderepeat.staycation.api.dataclass

import java.io.Serializable

data class Activity(
    val _id: String,
    val name: String,
    val type: String,
    val imageUrl: String
) : Serializable