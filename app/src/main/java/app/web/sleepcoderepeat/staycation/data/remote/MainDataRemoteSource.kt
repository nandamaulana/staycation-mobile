package app.web.sleepcoderepeat.staycation.data.remote

import android.annotation.SuppressLint
import android.util.Log
import app.web.sleepcoderepeat.staycation.api.ApiService
import app.web.sleepcoderepeat.staycation.api.dao.OrderDao
import app.web.sleepcoderepeat.staycation.data.MainDataSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.MultipartBody.Part.Companion.createFormData
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


object MainDataRemoteSource : MainDataSource {

    private val apiService = ApiService.create()

    override fun getLandingPageData(callback: MainDataSource.GetLandingPageDataCallback) {
        val services = apiService.getLandingPage()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                run {
                    if (it != null)
                        callback.onDataLoaded(it)
                    else
                        callback.onNotAvailable()
                }
            }, {
                callback.onError(it.message)
            })
        val compositeDisposable = CompositeDisposable()
        compositeDisposable.add(services)

    }

    @SuppressLint("CheckResult")
    override fun getDetailPageData(
        callback: MainDataSource.GetDetailPageDataCallback,
        _id: String
    ) {
        apiService.getDetailPage(_id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                run {
                    if (it != null)
                        callback.onDataLoaded(it)
                    else
                        callback.onNotAvailable()
                }
            }, {
                callback.onError(it.message)
            })
    }

    @SuppressLint("CheckResult")
    override fun submitCheckoutData(
        callback: MainDataSource.SubmitCheckoutDataCallback,
        orderDao: OrderDao,
        file: File
    ) {
        val fileReqBody: RequestBody = file.asRequestBody("image/jpg".toMediaTypeOrNull())
        val image: MultipartBody.Part =
            createFormData("image", file.name, fileReqBody)
        val orderDaoMap = HashMap<String, RequestBody>()
        with(orderDao) {
            orderDaoMap["idItem"] = idItem.toRequestBody("text/plain".toMediaTypeOrNull())
            orderDaoMap["duration"] = duration.toRequestBody("text/plain".toMediaTypeOrNull())
            orderDaoMap["bookingStartDate"] =
                bookingStartDate.toRequestBody("text/plain".toMediaTypeOrNull())
            orderDaoMap["bookingEndDate"] = bookingEndDate.toRequestBody("text/plain".toMediaTypeOrNull())
            orderDaoMap["firstName"] = firstName.toRequestBody("text/plain".toMediaTypeOrNull())
            orderDaoMap["lastName"] = lastName.toRequestBody("text/plain".toMediaTypeOrNull())
            orderDaoMap["email"] = email.toRequestBody("text/plain".toMediaTypeOrNull())
            orderDaoMap["phoneNumber"] = phoneNumber.toRequestBody("text/plain".toMediaTypeOrNull())
            orderDaoMap["accountHolder"] = accountHolder.toRequestBody("text/plain".toMediaTypeOrNull())
            orderDaoMap["bankFrom"] = bankFrom.toRequestBody("text/plain".toMediaTypeOrNull())
        }
        apiService.submitBooking(orderDaoMap,image)
            .enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    callback.onError("${t.message}")
                    t.printStackTrace()
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (response.isSuccessful) {
                        callback.onDataSubmitted(response.body().toString())
                        return
                    }
                    callback.onError(response.body().toString())
                }
            })
    }

}