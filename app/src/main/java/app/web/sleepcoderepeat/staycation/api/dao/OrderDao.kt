package app.web.sleepcoderepeat.staycation.api.dao

import java.io.Serializable

data class OrderDao (var idItem : String = "",//
                     var duration : String = "",//
                     var bookingStartDate : String = "",//
                     var bookingEndDate : String = "",//
                     var firstName : String = "",
                     var lastName : String = "",
                     var email : String = "",
                     var phoneNumber : String = "",
                     var accountHolder : String = "",
                     var bankFrom : String = "") : Serializable