package app.web.sleepcoderepeat.staycation.screen.checkout

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import app.web.sleepcoderepeat.staycation.api.dao.OrderDao
import app.web.sleepcoderepeat.staycation.api.dataclass.Bank
import app.web.sleepcoderepeat.staycation.api.dataclass.Item
import app.web.sleepcoderepeat.staycation.data.MainDataRepository
import app.web.sleepcoderepeat.staycation.data.MainDataSource
import app.web.sleepcoderepeat.staycation.screen.checkout.stepper.CheckoutFillDataStepFragment
import app.web.sleepcoderepeat.staycation.screen.checkout.stepper.CheckoutPickDateStepFragment
import app.web.sleepcoderepeat.staycation.screen.checkout.stepper.CheckoutSubmitPaymentStepFragment
import app.web.sleepcoderepeat.staycation.utils.*
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import java.io.File
import java.util.*


class CheckoutViewModel(
    application: Application,
    private val mainDataRepository: MainDataRepository
) :
    AndroidViewModel(application) {

    var orderDataField = MutableLiveData<OrderDao>()
    var selectedItem: ObservableField<Item> = ObservableField()
    val isBankExpanded = MutableLiveData<Boolean>()
    var sumBooking = MutableLiveData<String>()
    var checkoutDateRange = SingleLiveEvent<String>()
    val bankList: ObservableList<Bank> = ObservableArrayList()
    val paymentProofFile = SingleLiveEvent<File>()
    val grandTotal: String
        get() = (sumBooking.value!!.toDouble() + (sumBooking.value!!.toDouble() * 0.1)).toString()

    var stepperFragmentList: MutableList<StepData> = arrayListOf(
        StepData(
            CheckoutPickDateStepFragment(),
            "Booking Form",
            "Start Booking"
        ),
        StepData(
            CheckoutFillDataStepFragment(),
            "Booking Information",
            "Confirm Payment"
        ),
        StepData(
            CheckoutSubmitPaymentStepFragment(),
            "Submit Payment",
            "Submit"
        )
    )

    private fun initOrderDataField() {
        val tempOrderDao = OrderDao()
        with(tempOrderDao) {
            duration = INITIAL_DURATION
            idItem = selectedItem.get()!!._id
            bookingStartDate = Date().toStringFormatted()
            bookingEndDate = Date().sumDays(1).toStringFormatted()
        }
        orderDataField.value = tempOrderDao
        checkoutDateRange.value = getCheckoutDate()
        isBankExpanded.value = false
        paymentProofFile.value = File("")
        updateSumBooking()
    }

    fun setPaymentProofFile(file: File){
        paymentProofFile.value = file
    }

    fun toggleExpanded() {
        isBankExpanded.value = !isBankExpanded.value!!
    }

    fun plus() {
        val tempOrderDataField = orderDataField.value
        with(tempOrderDataField!!) {
            duration = (duration.toInt() + 1).toString()
            bookingEndDate = bookingStartDate.toDate().sumDays(duration.toInt()).toStringFormatted()
        }
        orderDataField.value = tempOrderDataField
        checkoutDateRange.value = getCheckoutDate()
        updateSumBooking()
    }

    fun minus() {
        val tempOrderDataField = orderDataField.value
        with(tempOrderDataField!!) {
            if (duration.toInt() > 1) {
                duration = (duration.toInt() - 1).toString()
                bookingEndDate =
                    bookingStartDate.toDate().sumDays(duration.toInt()).toStringFormatted()
            }
        }
        orderDataField.value = tempOrderDataField
        checkoutDateRange.value = getCheckoutDate()
        updateSumBooking()
    }


    fun setSelectedDate(startTimeMilis: Long, endTimeMilis: Long) {
        val tempOrderDataField = orderDataField.value
        val tempStartDate = Calendar.getInstance().setNoon()
        val tempEndDate = Calendar.getInstance().setNoon()
        with(tempOrderDataField!!) {
            bookingStartDate = tempStartDate.run {
                timeInMillis = startTimeMilis
                time.toStringFormatted()
            }
            bookingEndDate = tempEndDate.run {
                timeInMillis = endTimeMilis
                if (tempStartDate == tempEndDate)
                    add(Calendar.DATE, 1)
                time.toStringFormatted()
            }
            duration = tempEndDate.time.dayDiff(tempStartDate.time).toString()
        }

        orderDataField.value = tempOrderDataField
        checkoutDateRange.value = getCheckoutDate()
        updateSumBooking()
    }

    private fun updateSumBooking() {
        sumBooking.value =
            (orderDataField.value!!.duration.toInt() * selectedItem.get()!!.price.toInt()).toString()
    }

    fun getCheckoutDate(): String {
        return orderDataField.value!!.bookingStartDate
            .toDate()
            .getDateRangeWith(
                orderDataField.value!!.bookingEndDate.toDate()
            )
    }

    fun fillInformation(
        etEmail: String,
        etFirstName: String,
        etLastName: String,
        etPhoneNumber: String
    ): VerificationError? {
        val tempOrderDataField = orderDataField.value
        with(tempOrderDataField!!) {
            email = etEmail
            firstName = etFirstName
            lastName = etLastName
            phoneNumber = etPhoneNumber
        }
        orderDataField.value = tempOrderDataField
        return null
    }

    fun start(item: Item, bank: ArrayList<Bank>) {
        selectedItem.set(item)
        bankList.addAll(bank)
        initOrderDataField()
    }

    fun submitPayment(etBankFrom : String, etAccountHolder: String, callback: SubmitPaymentCallback){
        val tempOrderDataField = orderDataField.value
        with(tempOrderDataField!!){
            bankFrom = etBankFrom
            accountHolder = etAccountHolder
        }
        mainDataRepository.submitCheckoutData(object : MainDataSource.SubmitCheckoutDataCallback{
            override fun onDataSubmitted(response: String) {
                callback.onSuccess(response)
            }

            override fun onNotAvailable() {

            }

            override fun onError(msg: String?) {
                callback.onError(msg)
            }
        }, orderDataField.value!!, paymentProofFile.value!!)
    }

    companion object {
        private const val INITIAL_DURATION = "1"

        data class StepData(val step: Step, val titleStep: String, val nextText: String)
    }

}