package app.web.sleepcoderepeat.staycation.screen.detail

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import androidx.lifecycle.Observer
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.api.dataclass.Bank
import app.web.sleepcoderepeat.staycation.screen.checkout.CheckoutActivity
import app.web.sleepcoderepeat.staycation.utils.Constant
import app.web.sleepcoderepeat.staycation.utils.obtainViewModel
import app.web.sleepcoderepeat.staycation.utils.replaceFragmentInActivity
import java.util.ArrayList

class DetailActivity : AppCompatActivity() {

    lateinit var dActivity: AppCompatActivity
    lateinit var viewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        dActivity = this

        setupFragment()
        setupViewModel()

    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frameDetail)
        replaceFragmentInActivity(DetailFragment.newInstance(), R.id.frameDetail)
    }

    private fun setupViewModel() {
        viewModel = obtainViewModel().apply {
            openCheckout.observe(this@DetailActivity, Observer {
                val intent = Intent(this@DetailActivity, CheckoutActivity::class.java)

                intent.putExtra(Constant.OPEN_CHECKOUT_ITEM_KEY, it)
                intent.putParcelableArrayListExtra(Constant.BANK_LIST_KEY, detailDataField.get()!!.bank as ArrayList<Bank>)
                startActivity(intent)
            })
        }
    }

    fun obtainViewModel(): DetailViewModel = obtainViewModel(DetailViewModel::class.java)
}
