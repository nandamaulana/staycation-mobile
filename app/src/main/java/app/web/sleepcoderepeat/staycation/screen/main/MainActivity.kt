package app.web.sleepcoderepeat.staycation.screen.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.screen.detail.DetailActivity
import app.web.sleepcoderepeat.staycation.utils.Constant
import app.web.sleepcoderepeat.staycation.utils.obtainViewModel
import app.web.sleepcoderepeat.staycation.utils.replaceFragmentInActivity

class MainActivity : AppCompatActivity() {

    private lateinit var mActivity: AppCompatActivity
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mActivity = this

        setupFragment()
        setupViewModel()

    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frameMain)
        replaceFragmentInActivity(MainFragment.newInstance(), R.id.frameMain)
    }

    private fun setupViewModel() {
        viewModel = obtainViewModel().apply {
            openDetail.observe(this@MainActivity, Observer {
                val intent = Intent(this@MainActivity, DetailActivity::class.java)
                intent.putExtra(Constant.OPEN_DETAIL_ID_KEY, openDetail.value)
                startActivity(intent)
            })
        }
    }

    fun obtainViewModel(): MainViewModel = obtainViewModel(MainViewModel::class.java)

}
