package app.web.sleepcoderepeat.staycation.api.dataclass

import java.io.Serializable

data class Image(
    val _id: String,
    val imageUrl: String
) : Serializable