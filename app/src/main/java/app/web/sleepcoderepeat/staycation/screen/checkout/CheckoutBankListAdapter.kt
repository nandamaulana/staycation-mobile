package app.web.sleepcoderepeat.staycation.screen.checkout

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.api.dataclass.Bank
import app.web.sleepcoderepeat.staycation.api.dataclass.Category
import app.web.sleepcoderepeat.staycation.databinding.BankItemBinding
import app.web.sleepcoderepeat.staycation.databinding.CategoryBinding

class CheckoutBankListAdapter(
    private var bank: MutableList<Bank>

) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BankHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.bank_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = bank.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = bank[position]
        (holder as BankHolder).bindItem(item)

    }

    fun replaceData(items: MutableList<Bank>) {
        setList(items)
    }

    private fun setList(item: MutableList<Bank>) {
        this.bank = item
        notifyDataSetChanged()
    }

    class BankHolder(var binding: BankItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(bank: Bank) {
            binding.datas = bank
            binding.executePendingBindings()
        }
    }
}