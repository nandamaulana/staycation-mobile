package app.web.sleepcoderepeat.staycation.api.dataclass

import java.io.Serializable

data class Feature(
    val _id: String,
    val name: String,
    val qty: Int,
    val imageUrl: String
) : Serializable
