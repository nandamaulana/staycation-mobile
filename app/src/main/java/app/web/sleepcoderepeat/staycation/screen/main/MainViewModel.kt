package app.web.sleepcoderepeat.staycation.screen.main

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import app.web.sleepcoderepeat.staycation.api.dao.LandingDao
import app.web.sleepcoderepeat.staycation.api.dataclass.Category
import app.web.sleepcoderepeat.staycation.api.dataclass.Item
import app.web.sleepcoderepeat.staycation.data.MainDataRepository
import app.web.sleepcoderepeat.staycation.data.MainDataSource
import app.web.sleepcoderepeat.staycation.utils.SingleLiveEvent


class MainViewModel(application: Application, private val mainDataRepository: MainDataRepository) :
    AndroidViewModel(application) {

    val landingDataField: ObservableField<LandingDao> = ObservableField()
    val isLoading = MutableLiveData<Boolean>()
    val mostPicked: ObservableList<Item> = ObservableArrayList()
    val category: ObservableList<Category> = ObservableArrayList()
    internal val openDetail = SingleLiveEvent<String>()

    fun start() {
        getMainData()
    }

    fun openDetail(_id: String) {
        openDetail.value = _id
    }

    private fun getMainData() {
        isLoading.value = true
        mainDataRepository.getLandingPageData(object : MainDataSource.GetLandingPageDataCallback {
            override fun onDataLoaded(landingDao: LandingDao?) {
                isLoading.value = false
                landingDataField.set(landingDao)
                with(mostPicked) {
                    clear()
                    addAll(landingDao!!.mostPicked)
                }
                with(category){
                    clear()
                    addAll(landingDao!!.category)
                }
            }

            override fun onError(msg: String?) {
                isLoading.value = false
                Toast.makeText(getApplication(), "Error: $msg", Toast.LENGTH_LONG).show()
            }

            override fun onNotAvailable() {
                isLoading.value = false
                Toast.makeText(getApplication(), "Data not available", Toast.LENGTH_LONG).show()
            }
        })
    }

}