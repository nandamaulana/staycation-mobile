package app.web.sleepcoderepeat.staycation.utils

object Constant {
    const val BASE_URL = "https://api-staycation.herokuapp.com/"
//    const val BASE_URL = "http://192.168.1.10:3000/"
    const val BASE_ENDPOINT = "${BASE_URL}api/v1/member/"

    const val OPEN_DETAIL_ID_KEY = "_id"
    const val OPEN_CHECKOUT_ITEM_KEY = "item_checkout"
    const val BANK_LIST_KEY = "bank_list_key"


}