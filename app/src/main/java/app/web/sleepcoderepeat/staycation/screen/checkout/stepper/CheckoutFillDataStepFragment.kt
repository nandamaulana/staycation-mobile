package app.web.sleepcoderepeat.staycation.screen.checkout.stepper

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.annotation.UiThread
import androidx.fragment.app.Fragment
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.databinding.FragmentCheckoutStepFillDataBinding
import app.web.sleepcoderepeat.staycation.screen.checkout.CheckoutActivity
import com.stepstone.stepper.BlockingStep
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError

internal class CheckoutFillDataStepFragment : Fragment(), BlockingStep {

    lateinit var viewBinding: FragmentCheckoutStepFillDataBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewBinding = FragmentCheckoutStepFillDataBinding.inflate(inflater).apply {
            vm = (activity as CheckoutActivity).obtainViewModel()
        }
        viewBinding.lifecycleOwner = this

        return viewBinding.root
    }

    @UiThread
    override fun onBackClicked(callback: StepperLayout.OnBackClickedCallback) {
        callback.goToPrevStep()
    }

    override fun onSelected() {}

    override fun onCompleteClicked(callback: StepperLayout.OnCompleteClickedCallback) {
        callback.complete()
    }

    @UiThread
    override fun onNextClicked(callback: StepperLayout.OnNextClickedCallback) {
        callback.goToNextStep()
    }

    override fun verifyStep(): VerificationError? {
        with(viewBinding) {
            return if (isValid) viewBinding.vm!!.fillInformation(
                etEmail.text.toString(),
                etFirstName.text.toString(),
                etLastName.text.toString(),
                etPhoneNumber.text.toString()
            ) else VerificationError("Please Fill All Data!")
        }
    }

    private val isValid: Boolean
        get() {
            var status = true
            with(viewBinding) {
                tilEmail.error = null
                tilFirstName.error = null
                tilLastName.error = null
                tilPhoneNumber.error = null
                if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.text.toString()).matches()) {
                    tilEmail.error = "Email Format Not Valid"
                    tilEmail.startAnimation(
                        AnimationUtils.loadAnimation(
                            activity,
                            R.anim.shake_error
                        )
                    )
                    status = false
                }
                if (etFirstName.text.isNullOrBlank()) {
                    tilFirstName.error = "Please Fill First Name"
                    tilFirstName.startAnimation(
                        AnimationUtils.loadAnimation(
                            activity,
                            R.anim.shake_error
                        )
                    )
                    status = false
                }
                if (etLastName.text.isNullOrBlank()) {
                    tilLastName.error = "Please Fill Last Name"
                    tilLastName.startAnimation(
                        AnimationUtils.loadAnimation(
                            activity,
                            R.anim.shake_error
                        )
                    )
                    status = false
                }
                if (etPhoneNumber.text.isNullOrBlank()) {
                    tilPhoneNumber.error = "Please Fill Phone Number"
                    tilPhoneNumber.startAnimation(
                        AnimationUtils.loadAnimation(
                            activity,
                            R.anim.shake_error
                        )
                    )
                    status = false
                }
            }
            return status
        }

    override fun onError(error: VerificationError) {}

}