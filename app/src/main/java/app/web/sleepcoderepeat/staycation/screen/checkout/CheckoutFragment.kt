package app.web.sleepcoderepeat.staycation.screen.checkout


import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.api.dataclass.Item
import app.web.sleepcoderepeat.staycation.databinding.FragmentCheckoutBinding
import app.web.sleepcoderepeat.staycation.screen.checkout.stepper.CheckoutStepperAdapter
import app.web.sleepcoderepeat.staycation.utils.Constant
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import com.stepstone.stepper.internal.feedback.StepperFeedbackType
import kotlinx.android.synthetic.main.fragment_checkout.*

class CheckoutFragment : Fragment(), StepperLayout.StepperListener {

    lateinit var viewBinding: FragmentCheckoutBinding

    //    private var checkoutData: OrderDao? = null
    private var menu: Menu? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewBinding = FragmentCheckoutBinding.inflate(inflater).apply {
            vm = (activity as CheckoutActivity).obtainViewModel()
        }

        viewBinding.lifecycleOwner = this
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(viewBinding.toolbar as Toolbar)
        (viewBinding.toolbar as Toolbar).setNavigationOnClickListener { activity?.finish() }
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        setupStepperCheckout()

        viewBinding.vm?.start(
            requireActivity().intent!!.getSerializableExtra(Constant.OPEN_CHECKOUT_ITEM_KEY) as Item,
            requireActivity().intent!!.getParcelableArrayListExtra(Constant.BANK_LIST_KEY)!!
        )
    }

    private fun setupStepperCheckout() {
        viewBinding.stepperLayout.adapter = CheckoutStepperAdapter(
            requireActivity().supportFragmentManager,
            viewBinding.vm!!.stepperFragmentList,
            requireContext()
        )
        viewBinding.stepperLayout.setListener(this)
    }

    override fun onStepSelected(newStepPosition: Int) {}

    override fun onError(verificationError: VerificationError?) {}

    override fun onReturn() {
        activity?.finish()
    }

    override fun onCompleted(completeButton: View?) { }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.checkout_step_feedback, menu)
        this.menu = menu
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId

        if (!stepperLayout.isInProgress
            && (itemId == R.id.menu_feedback_content_progress
                    || itemId == R.id.menu_feedback_content_fade
                    || itemId == R.id.menu_feedback_content_overlay
                    || itemId == R.id.menu_feedback_tabs
                    || itemId == R.id.menu_feedback_nav
                    || itemId == R.id.menu_feedback_content_interaction)
        ) {
            toggleItem(item)
            val tabsEnabled = menu?.findItem(R.id.menu_feedback_tabs)?.isChecked
            val contentProgressEnabled =
                menu?.findItem(R.id.menu_feedback_content_progress)?.isChecked
            val contentFadeEnabled = menu?.findItem(R.id.menu_feedback_content_fade)?.isChecked
            val contentOverlayEnabled =
                menu?.findItem(R.id.menu_feedback_content_overlay)?.isChecked
            val disablingBottomNavigationEnabled = menu?.findItem(R.id.menu_feedback_nav)?.isChecked
            val disablingContentInteractionEnabled =
                menu?.findItem(R.id.menu_feedback_content_interaction)?.isChecked

            var feedbackMask = 0
            if (tabsEnabled == true) {
                feedbackMask = feedbackMask or StepperFeedbackType.TABS
            }
            if (contentProgressEnabled == true) {
                feedbackMask = feedbackMask or StepperFeedbackType.CONTENT_PROGRESS
            }
            if (contentFadeEnabled == true) {
                feedbackMask = feedbackMask or StepperFeedbackType.CONTENT_FADE
            }
            if (contentOverlayEnabled == true) {
                feedbackMask = feedbackMask or StepperFeedbackType.CONTENT_OVERLAY
            }
            if (disablingBottomNavigationEnabled == true) {
                feedbackMask = feedbackMask or StepperFeedbackType.DISABLED_BOTTOM_NAVIGATION
            }
            if (disablingContentInteractionEnabled == true) {
                feedbackMask = feedbackMask or StepperFeedbackType.DISABLED_CONTENT_INTERACTION
            }
            if (feedbackMask == 0) {
                feedbackMask = StepperFeedbackType.NONE
            }
            stepperLayout.setFeedbackType(feedbackMask)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun toggleItem(item: MenuItem) {
        item.isChecked = !item.isChecked
    }
}
