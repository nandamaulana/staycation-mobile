package app.web.sleepcoderepeat.staycation.screen.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.api.dataclass.Feature
import app.web.sleepcoderepeat.staycation.api.dataclass.Item
import app.web.sleepcoderepeat.staycation.databinding.BenefitItemBinding
import app.web.sleepcoderepeat.staycation.databinding.CategoryItemBinding

class DetailBenefitAdapter(
    private var benefitList: MutableList<Feature>,
    private var detailViewModel: DetailViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CategoryItemHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.benefit_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = benefitList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CategoryItemHolder).bindItem(benefitList[position])
    }

    fun replaceData(items: MutableList<Feature>) {
        setList(items)
    }

    private fun setList(item: MutableList<Feature>) {
        this.benefitList = item
        notifyDataSetChanged()
    }

    class CategoryItemHolder(var binding: BenefitItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(item: Feature) {
            binding.datas = item
            binding.executePendingBindings()
        }
    }
}