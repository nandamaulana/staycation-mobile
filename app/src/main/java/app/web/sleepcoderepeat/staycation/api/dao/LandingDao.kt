package app.web.sleepcoderepeat.staycation.api.dao

import app.web.sleepcoderepeat.staycation.api.dataclass.Category
import app.web.sleepcoderepeat.staycation.api.dataclass.Hero
import app.web.sleepcoderepeat.staycation.api.dataclass.Item
import app.web.sleepcoderepeat.staycation.api.dataclass.Testimonial

data class LandingDao(
    val hero: Hero,
    val mostPicked: MutableList<Item>,
    val category: MutableList<Category>,
    val testimonial: Testimonial
)












