package app.web.sleepcoderepeat.staycation.screen.checkout

import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import app.web.sleepcoderepeat.staycation.api.dataclass.Bank
import app.web.sleepcoderepeat.staycation.api.dataclass.Category
import app.web.sleepcoderepeat.staycation.api.dataclass.Item
import com.facebook.shimmer.ShimmerFrameLayout

object CheckoutBinding {
    @BindingAdapter("app:bankList")
    @JvmStatic
    fun setBankList(recyclerView: RecyclerView, bankList: MutableList<Bank>){
        with(recyclerView.adapter as CheckoutBankListAdapter){
            replaceData(bankList)
        }
    }


}