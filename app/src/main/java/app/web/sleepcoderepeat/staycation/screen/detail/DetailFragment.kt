package app.web.sleepcoderepeat.staycation.screen.detail


import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import app.web.sleepcoderepeat.staycation.R
import app.web.sleepcoderepeat.staycation.databinding.FragmentDetailBinding
import app.web.sleepcoderepeat.staycation.utils.Constant


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DetailFragment : Fragment() {

    private lateinit var viewBinding: FragmentDetailBinding
    private var sliderHandler: Handler = Handler()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewBinding = FragmentDetailBinding.inflate(inflater).apply {
            vm = (activity as DetailActivity).obtainViewModel()
        }
        viewBinding.lifecycleOwner = this
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupImageSlider()
        setupCategoryRecyclerView()
        (activity as AppCompatActivity).setSupportActionBar(viewBinding.toolbar as Toolbar)
        (viewBinding.toolbar as Toolbar).setNavigationOnClickListener { activity?.onBackPressed() }
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        viewBinding.vm?.start(requireActivity().intent!!.getStringExtra(Constant.OPEN_DETAIL_ID_KEY)!!)
    }

    private fun setupImageSlider() {
        val viewModel = viewBinding.vm
        if (viewModel != null) {
            viewBinding.rvBenefit.adapter = DetailBenefitAdapter(viewModel.benefit, viewModel)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        activity?.onBackPressed()
        return true
    }

    private fun setupCategoryRecyclerView() {
        val viewModel = viewBinding.vm
        if (viewModel != null) {
            viewBinding.vpImageSliderDetail.adapter = DetailImageSliderAdapter(
                viewModel.imageId,
                viewModel,
                viewBinding.vpImageSliderDetail
            )
        }
    }

    companion object {
        fun newInstance() = DetailFragment()
    }


}
